#include <Rainbowduino.h>

void setup()
{
  Rb.init();          //Initialise shield
  Serial.begin(9600); //Initialise Serial Port
}

void loop()
{
  //Read data from processing
  if (Serial.available()) {
    for (int i = 0; i < 8; i++) {
      dispLine(i, Serial.read()); //Get data from Processing
    }
  }
}
void dispLine(int i, int val) {
  uint32_t color[3] = {0x0000FF, 0x00FF00, 0xFF0000}; //Blue, Green, Red
  if (val == 0) {
    Rb.drawVerticalLine(i, 0, 8, 0); //Turn off unused LEDs
  } else {
    Rb.drawVerticalLine(i, 0, val - 1, color[1]); //Turn on LEDS under marker
    Rb.setPixelXY(i, val - 1, color[0]);          //Marker
    Rb.drawVerticalLine(i, val, 8 - val, 0);      //Turn off unused LEDs
  }
  delay(1);
}

